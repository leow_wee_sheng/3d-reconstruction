import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt

print "\n-------------------------------    PART 1. ----------------------------------"   

infile = open("md-data.txt")
data = np.genfromtxt(infile, delimiter=",")
data = np.matrix(data).T
infile.close()

#Q1 print out data to see the contents
print "\n-------------------------------    Q2. (Print data) ----------------------------------"   
print "Data: ", data.shape, "\n", data



def summation(j, d, total_value): 
   print "summing from ", j, " to ", d
   for i in range(j, d):
      total_value += eigvalues[index[i]]
   
   return total_value
   
#Get the number of dimensions d and number of data points n:
#d = 11, n = 30   each data point is 11 by 1
print "\n-------------------------------    Q3. (Print d and n) ----------------------------------"   
d = data.shape[0]
n = data.shape[1]
print "d: ", d , " n: ", n
print "d and n are correct."

mean = np.mean(data, 1)	#axis = 1 means horizontal

#Print mean to veritfy that it is a column vector e.g. only 1 row
print "\n-------------------------------    Q4. (Print mean) ----------------------------------"   
print "Mean: ", mean.shape, " \n", mean

#subtract data from data points to get the shifted datapoints
sdata = data - mean
print "\n-------------------------------    Q5. (Print mean of sdata) ----------------------------------"   
print "Mean of SData: ", np.mean(sdata, 1).shape, " \n", np.mean(sdata, 1)

#compute the covariance matrix of the shifted points
#numpy.cov(m, y=None, rowvar=1, bias=0, ddof=None)[source]
#sdata = shifted data points
#None = no additional set of variables and observations
cov = np.cov(sdata, None, 1, 1)		

#compute eigen de-composition
eigvalues, eigvectors = la.eig(cov)
print "eigvalues is: ", eigvalues

#np.argsort sorts the eigenvalues and returns an array of indices ordered
#in increasing order of eigenvalues.
index = np.argsort(eigvalues)[::-1]		#sort in desc order of eigenvalues


print "\n-------------------------------    PART 2. ----------------------------------"   
#d, dimension = 11
#l, reduced dimension = 1 to d

top_variance = 0
bottom_variance = 0

graph_x_points = []		# x-axis, R
graph_y_points = []		# y-axis, l

#j1 - top j, j2 - bottom j
for l in range(1,d):
   j1 = l+1
   j2 = 1
   
   print "\nl is: ", l
   top_variance = summation(j1, d, 0)

   print "top variance: ", top_variance
   bottom_variance = summation(j2, d, 0)

   print "bottom variance: ", bottom_variance
   R = top_variance / bottom_variance

   graph_x_points.append(l)
   graph_y_points.append(R)
   
   #print "Ratio R is: ", R
   
   
print "\n-------------------------------    Q2. (Plot a Graph, R vs l) ----------------------------------"   
#rounded graph 
#plt.plot(graph_x_points, graph_y_points, 'ro')
plt.plot(graph_x_points, graph_y_points)
plt.xlabel('number of dimensions L')
plt.ylabel('Ratio of unaccounted variance R')
plt.show()

#Q3. A smaller dimensions n can be represented to plot the graph
#l can be reduced from a smaller dimensions from 1 to 8 instead of 1 to 11
print "\n-------------------------------    Q3. (Select a smaller dimensions l) ----------------------------------"   
smaller_dimension = 8
print "Smaller dimensions l is: ", smaller_dimension

#Q4. Since previously eigvectors is sorted in descending order #eigvectors[index[0]] = largest eigenvectors and so on

print "\n-------------------------------    Q4. (Compute and Print matrix Q) ----------------------------------"   
QList = []
# list Q sorted in order of eigenvectors
for i in range(0,d):
   QList.append( eigvectors[index[i]] )	
 
Q = np.matrix(QList)
print "Q matrix ", Q.shape, "\n", Q


xi_minus_m = np.matrix( sdata )

#print "X-m: ", xi_minus_m
#print "Q matrix: ", Q.shape

print "\n-------------------------------    Q5. (Compute yi vector) ----------------------------------"   
yi = Q.T * xi_minus_m
print "YI matrix dimension: ", yi.shape		# 11 x 30

#x0 = data[0,:]
#y0 = Q.T * (x0

reduced_yi = yi[:smaller_dimension]

print "\n-------------------------------    Q6. (Compute and Print reduced dimensional vectors yi^) ----------------------------------"
print "reduced dimensional vector yi ", reduced_yi.shape, "\n", reduced_yi


reduced_Q = Q[:smaller_dimension]
print "\n-------------------------------    Q7. (Compute and Print reduced dimensional matrix Q^) ----------------------------------"
print "reduced dimensional matrix Q ", reduced_Q.shape, "\n", reduced_Q


print "\n-------------------------------    Q8. (Compute and Estimates data points from Q^ to Y^) ----------------------------------"
reduced_xi = reduced_Q.T * reduced_yi + mean
print "estimated xi ", reduced_xi.shape, "\n", reduced_xi


#la.norm( M * a - b ) * la.norm( M * a - b )
print "\n-------------------------------    Q9. (Compute Square-root of Squared Difference between xi and xi^) ----------------------------------"
#difference = la.norm( data - reduced_xi ) * la.norm( data - reduced_xi )
difference = la.norm( data - reduced_xi )
print "difference ", difference



#### PAUSE #### DO UNTIL QN 9 AND STOP



