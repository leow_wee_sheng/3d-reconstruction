import cv2
import cv2.cv as cv
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt
import math

os.chdir("/Users/LeowWeeSheng/Documents/CS4243/lab5")

#Part 1

infile = open("md-data.txt")
data = np.genfromtxt(infile, delimiter=",")
data = np.matrix(data).T
infile.close()
#print data

d = data.shape[0]
n = data.shape[1]
#print d
#print n

mean = np.mean(data,1)
#print mean

sdata = data - mean
#print np.mean(sdata, 1)

cov = np.cov(sdata, None, 1, 1)

eigvalues, eigvectors = la.eig(cov)
index = np.argsort(eigvalues)
desc = index[::-1] # reverse index

#Part 2
unaccounted = []
l=4
bottom = sum(eigvalues)
for i in index:
	if i < l:
		unaccounted.append(eigvalues[index[i]])
top = sum(unaccounted)
R = float(top) / float(bottom)
print "R: ", R

#plot graph R
plt.plot(unaccounted)
#plt.axis([0,d,0,2])
plt.ylabel('R')
plt.show()

#plot graph l
accounted = []
for i in index:
	if i >= l:
		accounted.append(eigvalues[index[i]])
plt.plot(accounted)
plt.ylabel('l')
plt.show()

#plot graph
plt.plot(eigvalues)
plt.ylabel('eigenvalues')
plt.show()

eigvalues = eigvalues[index]
eigvectors = eigvectors[:,index]
Q = np.matrix(eigvectors)

#Qn5
yi = np.transpose(Q)*sdata

#Qn6
yi_norm = la.norm(yi)
print yi_norm

#Qn7
for i in index:
	if i >= l:
		Q_norm = Q[index[i]]
print Q_norm

#Qn8
xi_norm = Q_norm*yi_norm + mean
print la.norm(xi_norm)
