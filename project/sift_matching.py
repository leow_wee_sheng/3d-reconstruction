#reference: https://github.com/jayrambhia/Vision/blob/master/OpenCV/Python/sift_matching.py

import cv2
import numpy as np
import itertools
import sys

def computeFeatures(img, distance=200):
	detector = cv2.FeatureDetector_create("SIFT") # creates a SIFT detector
	descriptor = cv2.DescriptorExtractor_create("SIFT") # creates a SIFT descriptor to extract keypoints
	
	#skp is a list of all the keypoints found on the image 
	#sd is the descriptor for the image
	kp = detector.detect(img)
	kp, d = descriptor.compute(img, kp)
	return kp, d
	

def findKeyPoints(img, template, distance=200):
    detector = cv2.FeatureDetector_create("SIFT") # creates a SIFT detector
    descriptor = cv2.DescriptorExtractor_create("SIFT") # creates a SIFT descriptor to extract keypoints
	
	#skp is a list of all the keypoints found on the image 
	#sd is the descriptor for the image
    skp = detector.detect(img)
    skp, sd = descriptor.compute(img, skp)
	
	#tkp is a list of all the keypoints found on the template
	#td is the descriptor for the template
    tkp = detector.detect(template)
    tkp, td = descriptor.compute(template, tkp)
	
	#Matching Keypoints using FLANN based detector
	
	#This method does a fast local approximate nearest neighbors (FLANN) calculation between two sets of feature vectors. 
	#The result are two numpy arrays the first one is a list of indexes of the matches and the second one is the match distance value. 
	#For the match indices or idx, the index values correspond to the values of td, and the value in the array is the index in td.
	
    flann_params = dict(algorithm=1, trees=4)
    flann = cv2.flann_Index(sd, flann_params)
    idx, dist = flann.knnSearch(td, 1, params={})
    del flann

    dist = dist[:,0]/2500.0
    dist = dist.reshape(-1,).tolist()
    idx = idx.reshape(-1).tolist()
    indices = range(len(dist))
    indices.sort(key=lambda i: dist[i])
    dist = [dist[i] for i in indices]
    idx = [idx[i] for i in indices]
    skp_final = []
    for i, dis in itertools.izip(idx, dist):
        if dis < distance:
            skp_final.append(skp[i])

    flann = cv2.flann_Index(td, flann_params)
    idx, dist = flann.knnSearch(sd, 1, params={})
    del flann
	
	#Sorting keypoint matches according to the distance
    dist = dist[:,0]/2500.0
    dist = dist.reshape(-1,).tolist()
    idx = idx.reshape(-1).tolist()
    indices = range(len(dist))
    indices.sort(key=lambda i: dist[i])
    dist = [dist[i] for i in indices]
    idx = [idx[i] for i in indices]
	
	#After sorting the keypoints according to the distance, filter keypoints by setting a cut-off limit of distance.
    tkp_final = []
    for i, dis in itertools.izip(idx, dist):
        if dis < distance:
            tkp_final.append(tkp[i])

    return skp_final, tkp_final

def drawKeyPoints(img, template, skp, tkp, num=-1):
    h1, w1 = img.shape[:2]
    h2, w2 = template.shape[:2]
    nWidth = w1+w2
    nHeight = max(h1, h2)
    hdif = (h1-h2)/2
    newimg = np.zeros((nHeight, nWidth, 3), np.uint8)
    newimg[hdif:hdif+h2, :w2] = template
    newimg[:h1, w2:w1+w2] = img

    maxlen = min(len(skp), len(tkp))
    if num < 0 or num > maxlen:
        num = maxlen
    for i in range(num):
        pt_a = (int(tkp[i].pt[0]), int(tkp[i].pt[1]+hdif))
        pt_b = (int(skp[i].pt[0]+w2), int(skp[i].pt[1]))
        cv2.line(newimg, pt_a, pt_b, (255, 0, 0))
    return newimg


def match(image, template):
	img = cv2.imread(image)
	temp = cv2.imread(template)
	
	try:
		dist = int(sys.argv[3])
	except IndexError:
		dist = 200
	try:
		num = int(sys.argv[4])
	except IndexError:
		num = -1
		
	skp, tkp = findKeyPoints(img, temp, dist)
	newimg = drawKeyPoints(img, temp, skp, tkp, num)
	"""
	cv2.imshow('matched',newimg)
	cv2.waitKey(0)
	"""
	return newimg