import Image
import os
import numpy as np
import harris

def process_image(imagename,resultname,params="--edge-thresh 10 --peak-thresh 5"):
	""" Process an image and save the results in a file. """

	if imagename[-3:] != 'pgm':
		# create a pgm file
		im = Image.open(imagename).convert('L')
		im.save('tmp.pgm')
		imagename = 'tmp.pgm'

	cmmd = str("sift "+imagename+" --output="+resultname+" "+params)
	os.system(cmmd)
	print 'processed', imagename, 'to', resultname

def read_features_from_file(filename):
	""" Read feature properties and return in matrix form. """
	f = np.loadtxt(filename)
	return f[:,:4],f[:,4:] # feature locations, descriptors
	
def write_features_to_file(filename,locs,desc):
	""" Save feature location and descriptor to file. """
	savetxt(filename,hstack((locs,desc)))
	
def plot_features(im,locs,circle=False):
	""" Show image with features. input: im (image as array),
	locs (row, col, scale, orientation of each feature). """
	
	def draw_circle(c,r):
		t = arange(0,1.01,.01)*2*pi
		x = r*cos(t) + c[0]
		y = r*sin(t) + c[1]
		plot(x,y,'b',linewidth=2)
	
	imshow(im)
	if circle:
		for p in locs:
			draw_circle(p[:2],p[2])
	else:
		plot(locs[:,0],locs[:,1],'ob')
	axis('off')
	
def match_twosided(desc1,desc2,threshold=0.5):
	""" Two-sided symmetric version of match(). """
	
	matches_12 = harris.match(desc1,desc2,threshold)
	matches_21 = harris.match(desc2,desc1,threshold)
	
	ndx_12 = where(matches_12 >= 0)[0]
	
	# remove matches that are not symmetric
	for n in ndx_12:
		if matches_21[matches_12[n]] != n:
			matches_12[n] = -1
			
	return matches_12