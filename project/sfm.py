def compute_fundamental(x1,x2):
	""" Computes the fundamental matrix from corresponding points
	(x1,x2 3*n arrays) using the normalized 8 point algorithm.
	each row is constructed as
	[x'*x, x'*y, x', y'*x, y'*y, y', x, y, 1] """
	
	n = x1.shape[1]
	if x2.shape[1] != n:
		raise ValueError("Number of points don't match.")
		
	# build matrix for equations
	A = zeros((n,9))
	for i in range(n):
		A[i] = [x1[0,i]*x2[0,i], x1[0,i]*x2[1,i], x1[0,i]*x2[2,i],x1[1,i]*x2[0,i], x1[1,i]*x2[1,i], 
			x1[1,i]*x2[2,i],x1[2,i]*x2[0,i], x1[2,i]*x2[1,i], x1[2,i]*x2[2,i] ]
		
	# compute linear least square solution
	U,S,V = linalg.svd(A)
	F = V[-1].reshape(3,3)
	
	# constrain F
	# make rank 2 by zeroing out last singular value
	U,S,V = linalg.svd(F)
	S[2] = 0
	F = dot(U,dot(diag(S),V))
	
	return F

def compute_epipole(F):
	""" Computes the (right) epipole from a fundamental matrix F. (Use with F.T for left epipole.) """
	# return null space of F (Fx=0)
	U,S,V = linalg.svd(F)
	e = V[-1]
	return e/e[2]
	
def compute_P_from_essential(E):
	""" Computes the second camera matrix (assuming P1 = [I 0])
	from an essential matrix. Output is a list of four
	possible camera matrices. """
	
	# make sure E is rank 2
	U,S,V = svd(E)
	if det(dot(U,V))<0:
		V = -V
	E = dot(U,dot(diag([1,1,0]),V))
	
	# create matrices (Hartley p 258)
	Z = skew([0,0,-1])
	W = array([[0,-1,0],[1,0,0],[0,0,1]])
	
	# return all four solutions
	P2 = [vstack((dot(U,dot(W,V)).T,U[:,2])).T,
		vstack((dot(U,dot(W,V)).T,-U[:,2])).T,
		vstack((dot(U,dot(W.T,V)).T,U[:,2])).T,
		vstack((dot(U,dot(W.T,V)).T,-U[:,2])).T]
		
	return P2
	
def compute_P(x,X):
	""" Compute camera matrix from pairs of
	2D-3D correspondences (in homog. coordinates). """
	
	n = x.shape[1]
	if X.shape[1] != n:
		raise ValueError("Number of points don't match.")
		
	# create matrix for DLT solution
	M = zeros((3*n,12+n))
	for i in range(n):
		M[3*i,0:4] = X[:,i]
		M[3*i+1,4:8] = X[:,i]
		M[3*i+2,8:12] = X[:,i]
		M[3*i:3*i+3,i+12] = -x[:,i]
		
	U,S,V = linalg.svd(M)
	
	return V[-1,:12].reshape((3,4))

class RansacModel(object):
	""" Class for fundmental matrix fit with ransac.py from
	http://www.scipy.org/Cookbook/RANSAC"""
	
	def __init__(self,debug=False):
		self.debug = debug
		
	def fit(self,data):
		""" Estimate fundamental matrix using eight
		selected correspondences. """
		
		# transpose and split data into the two point sets
		data = data.T
		x1 = data[:3,:8]
		x2 = data[3:,:8]
		# estimate fundamental matrix and return
		F = compute_fundamental_normalized(x1,x2)
		return F
		
	def get_error(self,data,F):
		""" Compute x^T F x for all correspondences,
		return error for each transformed point. """
		
		# transpose and split data into the two point
		data = data.T
		x1 = data[:3]
		x2 = data[3:]
		
		# Sampson distance as error measure
		Fx1 = dot(F,x1)
		Fx2 = dot(F,x2)
		denom = Fx1[0]**2 + Fx1[1]**2 + Fx2[0]**2 + Fx2[1]**2
		err = ( diag(dot(x1.T,dot(F,x2))) )**2 / denom
		
		# return error per point
		return err
	
def F_from_ransac(x1,x2,model,maxiter=5000,match_theshold=1e-6):
	""" Robust estimation of a fundamental matrix F from point
	correspondences using RANSAC (ransac.py from
	http://www.scipy.org/Cookbook/RANSAC).
	input: x1,x2 (3*n arrays) points in hom. coordinates. """
	
	import ransac
	
	data = vstack((x1,x2))
	# compute F and return with inlier index
	F,ransac_data = ransac.ransac(data.T,model,8,maxiter,match_theshold,20,return_all=True)
	
	return F, ransac_data['inliers']