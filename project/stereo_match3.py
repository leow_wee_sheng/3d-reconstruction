'''
Simple example of stereo image matching and point cloud generation.

Resulting .ply file cam be easily viewed using MeshLab (http://meshlab.sourceforge.net/)
'''

import sys
from cgkit.all import *
import numpy as np
import cv2

ply_header = "ply\n\
format ascii 1.0\n\
element vertex %(vert_num)d\n\
property float x\n\
property float y\n\
property float z\n\
property uchar red\n\
property uchar green\n\
property uchar blue\n\
end_header\n"

def write_ply(fn, verts, colors):
    verts = verts.reshape(-1, 3)
    colors = colors.reshape(-1, 3)
    verts = np.hstack([verts, colors])
    with open(fn, 'wb') as f:
        f.write(ply_header % dict(vert_num=len(verts)))
        np.savetxt(f, verts, '%f %f %f %d %d %d')


print 'loading images...'
img = []
'''
img.append(cv2.pyrDown(cv2.imread('house.000.bmp')))
img.append(cv2.pyrDown(cv2.imread('house.001.bmp')))
img.append(cv2.pyrDown(cv2.imread('house.002.bmp')))
img.append(cv2.pyrDown(cv2.imread('house.003.bmp')))
img.append(cv2.pyrDown(cv2.imread('house.004.bmp')))
img.append(cv2.pyrDown(cv2.imread('house.005.bmp')))
img.append(cv2.pyrDown(cv2.imread('house.006.bmp')))
img.append(cv2.pyrDown(cv2.imread('house.007.bmp')))
img.append(cv2.pyrDown(cv2.imread('house.008.bmp')))
img.append(cv2.pyrDown(cv2.imread('house.009.bmp')))
'''

img.append(cv2.pyrDown(cv2.imread('viff.000.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.001.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.002.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.003.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.004.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.005.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.006.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.007.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.008.bmp')))
'''
img.append(cv2.pyrDown(cv2.imread('viff.009.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.010.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.011.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.012.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.013.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.014.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.015.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.016.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.017.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.018.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.019.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.020.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.021.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.022.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.023.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.024.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.025.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.026.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.027.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.028.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.029.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.030.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.031.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.032.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.033.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.034.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.035.bmp')))
img.append(cv2.pyrDown(cv2.imread('viff.036.bmp')))
'''

# disparity range is tuned for 'aloe' image pair
window_size = 3
min_disp = 16
num_disp = 112-min_disp
stereo = cv2.StereoSGBM(minDisparity = min_disp, 
    numDisparities = num_disp, 
    SADWindowSize = window_size,
    uniquenessRatio = 30,
    speckleWindowSize = 100,
    speckleRange = 6,
    disp12MaxDiff = 6,
    P1 = 8*3*window_size**2,
    P2 = 32*3*window_size**2,
    fullDP = False
)

print 'computing disparity...'
disp = []
disp_total = 0
for i in range(0, len(img)-1):
	disp.append(stereo.compute(img[i], img[i+1]).astype(np.float32) / 16.0)
	disp_total += disp[i] 

print len(disp_total)
disp_mean = disp_total / len(disp)
print 'generating 3d point cloud...',
h, w = img[0].shape[:2]
f = 0.5*w                          # guess for focal length
Q = np.float32([[1, 0, 0, -0.5*w],
                [0,-1, 0,  0.5*h], # turn points 180 deg around x-axis, 
                [0, 0, 0,     -f], # so that y-axis looks up
                [0, 0, 1,      0]])
points = cv2.reprojectImageTo3D(disp_mean, Q)
colors = cv2.cvtColor(img[0], cv2.COLOR_BGR2RGB)
mask = disp_mean > disp_mean.min()
out_points = points[mask]
out_colors = colors[mask]
out_fn = 'output.ply'
write_ply('output.ply', out_points, out_colors)
print '%s saved' % 'out.ply'

#cv2.imshow('left', imgL)
#cv2.imshow('disparity', (disp-min_disp)/num_disp)

print 'importing to cgkit...'
load("output.ply")
proj = worldObject("output")
print 'opening viewer...'

L = proj.worldtransform
#for v in proj.geom.verts:
#    drawMarker(L*v, size=0.07, color=(0,0,1))
out_points = out_points.reshape(-1, 3)
colors = colors.reshape(-1, 3)

for i,v in enumerate(out_points):
    drawMarker(L*vec3(float(v[0]), float(v[1]), float(v[2])), size=0.07, color=(float(out_colors[i][0])/255, float(out_colors[i][1])/255, float(out_colors[i][2])/255))
    
