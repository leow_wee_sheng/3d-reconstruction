import cv2
import numpy as np
from numpy import linalg as la
from matplotlib import pyplot as plt
from numpy import *
from cgkit.all import *

"""
STEPS:
detect keypoints (SURF, SIFT) => extract descriptors (SURF,SIFT) => compare and match descriptors (BruteForce, Flann based approaches) =>
find fundamental mat (findFundamentalMat()) from these pairs => stereoRectifyUncalibrated() => reprojectImageTo3D()
"""

def compute_fundamental(x1,x2):
	""" Computes the fundamental matrix from corresponding points (x1,x2 3*n arrays) using the normalized 8 point algorithm. each row is constructed as [x'*x, x'*y, x', y'*x, y'*y, y', x, y, 1] """
	n = x1.shape[1]
	if x2.shape[1] != n:
		raise ValueError("Number of points don't match.")
	# build matrix for equations
	A = zeros((n,9))
	for i in range(n):
		A[i] = [x1[0,i]*x2[0,i], x1[0,i]*x2[1,i], x1[0,i]*x2[2,i],
			x1[1,i]*x2[0,i], x1[1,i]*x2[1,i], x1[1,i]*x2[2,i],
			x1[2,i]*x2[0,i], x1[2,i]*x2[1,i], x1[2,i]*x2[2,i] ]
	# compute linear least square solution
	U,S,V = linalg.svd(A)
	F = V[-1].reshape(3,3)
	# constrain F
	# make rank 2 by zeroing out last singular value
	U,S,V = linalg.svd(F)
	S[2] = 0
	F = dot(U,dot(diag(S),V))
	return F

def compute_epipole(F):
	""" Computes the (right) epipole from a fundamental matrix F. (Use with F.T for left epipole.) """
	# return null space of F (Fx=0)
	U,S,V = la.svd(F)
	e = V[-1]
	return e/e[2]

def compute_P_from_fundamental(F):
	""" Computes the second camera matrix (assuming P1 = [I 0]) from a fundamental matrix. """
	e = compute_epipole(F.T) # left epipole
	Te = skew(e)
	return vstack((dot(Te,F.T).T,e)).T
	
def skew(a):
	""" Skew matrix A such that a x v = Av for any v. """
	return array([[0,-a[2],a[1]],[a[2],0,-a[0]],[-a[1],a[0],0]])
	
def triangulate_point(x1,x2,P1,P2):
	""" Point pair triangulation from least squares solution. """
	M = zeros((6,6))
	M[:3,:4] = P1
	M[3:,:4] = P2
	M[:3,4] = -x1
	M[3:,5] = -x2
	U,S,V = la.svd(M)
	X = V[-1,:4]
	return X / X[3]
	
def triangulate(x1,x2,P1,P2):
	""" Two-view triangulation of points in x1,x2 (3*n homog. coordinates). """
	n = x1.shape[1]
	if x2.shape[1] != n:
		raise ValueError("Number of points don't match.")
	X = [ triangulate_point(x1[:,i],x2[:,i],P1,P2) for i in range(n)]
	return array(X).T

def drawlines(img1,img2,lines,pts1,pts2):
    ''' img1 - image on which we draw the epilines for the points in img2
        lines - corresponding epilines '''
    r,c = img1.shape
    img1 = cv2.cvtColor(img1,cv2.COLOR_GRAY2BGR)
    img2 = cv2.cvtColor(img2,cv2.COLOR_GRAY2BGR)
    for r,pt1,pt2 in zip(lines,pts1,pts2):
        color = tuple(np.random.randint(0,255,3).tolist())
        x0,y0 = map(int, [0, -r[2]/r[1] ])
        x1,y1 = map(int, [c, -(r[2]+r[0]*c)/r[1] ])
        cv2.line(img1, (x0,y0), (x1,y1), color,1)
        cv2.circle(img1,tuple(pt1),5,color,-1)
        cv2.circle(img2,tuple(pt2),5,color,-1)
    return img1,img2

def write_ply(fn, verts, colors):
    verts = verts.reshape(-1, 3)
    colors = colors.reshape(-1, 3)
    verts = np.hstack([verts, colors])
    with open(fn, 'wb') as f:
        f.write(ply_header % dict(vert_num=len(verts)))
        np.savetxt(f, verts, '%f %f %f %d %d %d')

ply_header = "ply\n\
format ascii 1.0\n\
element vertex %(vert_num)d\n\
property float x\n\
property float y\n\
property float z\n\
property uchar red\n\
property uchar green\n\
property uchar blue\n\
end_header\n"

img1 = cv2.imread('viff.000.bmp',0)  #queryimage # left image
img2 = cv2.imread('viff.003.bmp',0) #trainimage # right image

sift = cv2.SIFT()

# find the keypoints and descriptors with SIFT
kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)

# FLANN parameters
FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks=50)

flann = cv2.FlannBasedMatcher(index_params,search_params)
matches = flann.knnMatch(des1,des2,k=2)

good = []
pts1 = []
pts2 = []

# ratio test as per Lowe's paper
for i,(m,n) in enumerate(matches):
    if m.distance < 0.8*n.distance:
        good.append(m)
        pts2.append(kp2[m.trainIdx].pt)
        pts1.append(kp1[m.queryIdx].pt)
		
pts1 = np.float32(pts1)
pts2 = np.float32(pts2)
F, mask = cv2.findFundamentalMat(pts1,pts2,cv2.FM_LMEDS)
F1 = compute_fundamental(pts1,pts2)
#epipole = compute_epipole(F)

# We select only inlier points
#pts1 = pts1[mask.ravel()==1]
#pts2 = pts2[mask.ravel()==1]
	
# Find epilines corresponding to points in right image (second image) and
# drawing its lines on left image
lines1 = cv2.computeCorrespondEpilines(pts2.reshape(-1,1,2), 2,F)
lines1 = lines1.reshape(-1,3)
img5,img6 = drawlines(img1,img2,lines1,pts1,pts2)

# Find epilines corresponding to points in left image (first image) and
# drawing its lines on right image
lines2 = cv2.computeCorrespondEpilines(pts1.reshape(-1,1,2), 1,F)
lines2 = lines2.reshape(-1,3)
img3,img4 = drawlines(img2,img1,lines2,pts2,pts1)

plt.subplot(121),plt.imshow(img5)
plt.subplot(122),plt.imshow(img3)
#plt.show()

pts1 = np.int32(pts1)
pts2 = np.int32(pts2)

retval,H1,H2 = cv2.stereoRectifyUncalibrated(pts1.ravel(), pts2.ravel(), F, img1.shape)

H1 = np.float32(H1)
H2 = np.float32(H2)

print 'generating 3d point cloud...',
h, w = img1.shape[:2]
f = 0.5*w                          # guess for focal length
Q = np.float32([[1, 0, 0, -0.5*w],
                [0,-1, 0,  0.5*h], # turn points 180 deg around x-axis, 
                [0, 0, 0,     -f], # so that y-axis looks up
                [0, 0, 1,      0]])

points = cv2.reprojectImageTo3D(H1, Q)
out_points = points[mask]

'''
colors = img1
out_points = points[mask]
out_colors = colors[mask]
out_fn = 'output.ply'
write_ply('output.ply', out_points, out_colors)
'''

#print 'importing to cgkit...'
#load("output.ply")
#proj = worldObject("output")
print 'opening viewer...'
out_points = out_points.reshape(-1, 3)
for i in out_points:
	i[0] /= 20000
	i[1] /= 20000
	i[2] /= 20000

L = vec3(1/25000)

for i in range(0, len(out_points) - 1):
	pnt = vec3(double(out_points[i][0]), double(out_points[i][1]), double(out_points[i][2]))
	print pnt
	drawMarker((pnt), size=0.07, color=(0,0,1))