import numpy as np
import cv2
import cv2.cv as cv
import sfm
import sift_matching
import numpy.linalg as la
import sys

# this file is using opencv for 3d reconstruction

# calibration
K = np.array([[2394,0,932],[0,2398,628],[0,0,1]])
'''
#Reference purpose, do not remove

# load images and compute features
img1 = cv2.imread('viff.000.bmp')
sift1 = cv2.SIFT()
kp1, des1 = sift1.detectAndCompute(img1,None)
#l1, d1 = sift_matching.computeFeatures(img1)

img2 = cv2.imread('viff.003.bmp')
sift2 = cv2.SIFT()
kp2, des2 = sift2.detectAndCompute(img2,None)
#l2, d2 = sift_matching.computeFeatures(img2)

# match features (Flann Based matches)
# FLANN parameters
FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks=50)   # or pass empty dictionary

flann = cv2.FlannBasedMatcher(index_params,search_params)
matches = flann.knnMatch(des1,des2,k=2)

# Need to draw only good matches, so create a mask
matchesMask = [[0,0] for i in xrange(len(matches))]

good = []
pts1 = []
pts2 = []

# ratio test as per Lowe's paper
for i,(m,n) in enumerate(matches):
    if m.distance < 0.8*n.distance:
        good.append(m)
        pts2.append(kp2[m.trainIdx].pt)
        pts1.append(kp1[m.queryIdx].pt)
		
pts1 = np.float32(pts1)
pts2 = np.float32(pts2)
F, mask = cv2.findFundamentalMat(pts1,pts2,cv2.FM_LMEDS)

# We select only inlier points
#pts1 = pts1[mask.ravel()==1]
#pts2 = pts2[mask.ravel()==1]

#img3 = cv2.drawMatches(img1,kp1,img2,kp2,matches,None,**draw_params)
#cv2.imwrite('im3.jpg',img2)
'''
# visualization
matchImg = sift_matching.match('viff.000.bmp','viff.003.bmp')
ndx = matchImg.nonzero()[0]
cv2.imwrite('match.jpg',matchImg)
'''
# make homogeneous and normalize with inv(K)
x1 = cv2.convertPointsToHomogeneous(pts1[ndx])
ndx2 = [int(matchImg[i]) for i in ndx]
x2 = cv2.convertPointsToHomogeneous(pts2[ndx2])

x1n = np.dot(la.inv(K),x1)
x2n = np.dot(la.inv(K),x2)
'''
"""
x1 = homography.make_homog(l1[ndx,:2].T)
ndx2 = [int(matches[i]) for i in ndx]
x2 = homography.make_homog(l2[ndx2,:2].T)

x1n = dot(inv(K),x1)
x2n = dot(inv(K),x2)

# estimate E with RANSAC
model = sfm.RansacModel()
E,inliers = sfm.F_from_ransac(x1n,x2n,model)

# compute camera matrices (P2 will be list of four solutions)
P1 = np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0]])
P2 = sfm.compute_P_from_essential(E)

# pick the solution with points in front of cameras
ind = 0
maxres = 0
for i in range(4):
  # triangulate inliers and compute depth for each camera
  X = sfm.triangulate(x1n[:,inliers],x2n[:,inliers],P1,P2[i])
  d1 = dot(P1,X)[2]
  d2 = dot(P2[i],X)[2]
  if sum(d1>0)+sum(d2>0) > maxres:
    maxres = sum(d1>0)+sum(d2>0)
    ind = i
    infront = (d1>0) & (d2>0)


# triangulate inliers and remove points not in front of both cameras
X = sfm.triangulate(x1n[:,inliers],x2n[:,inliers],P1,P2[ind])
X = X[:,infront]

# 3D plot
from mpl_toolkits.mplot3d import axes3d

fig = figure()
ax = fig.gca(projection='3d')
ax.plot(-X[0],X[1],X[2],'k.')
axis('off')

# plot the projection of X
import camera

# project 3D points
cam1 = camera.Camera(P1)
cam2 = camera.Camera(P2[ind])
x1p = cam1.project(X)
x2p = cam2.project(X)

# reverse K normalization
x1p = dot(K,x1p)
x2p = dot(K,x2p)

figure()
imshow(im1)
gray()
plot(x1p[0],x1p[1],'o')
plot(x1[0],x1[1],'r.')
axis('off')

figure()
imshow(im2)
gray()
plot(x2p[0],x2p[1],'o')
plot(x2[0],x2[1],'r.')
axis('off')
show()
"""