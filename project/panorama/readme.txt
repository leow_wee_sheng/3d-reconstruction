This Python program reads in a series of images and stitch them into a panorama image.

External libraries required:
1. opencv2
2. numpy

The program takes in 4 inputs:
1. Output image directory
2. Source image directory
3. Input image file format
3. 'Base Image' file name

Points to note:
1. The program only reads the file format stated in the input. Ensure that there are no other images with the same extension in the folder.
2. The 'Base Image' is preferably the centre image of the series of input images. This is to create lesser distortion. However, the program will work perfectly for base images at any positions.
3. When the panorama gets too large to process (i.e. too many feature points), the program will stop and return the latest panorama image.

Acknowledgements:
1. http://docs.opencv.org/doc/tutorials/features2d/feature_homography/feature_homography.html
2. https://github.com/cbuntain/stitcher/blob/master/alignImagesRansac.py