import os
import sys
import cv2
import math
import numpy as np

from numpy import linalg

def resizeImage(image):
    img = cv2.imread(image)
    h,w,z = img.shape
    larger = h if h > w else w
    ceiling = math.ceil(larger/1000.0) if larger > 1000 else 1.0
    out_img = cv2.resize(img, (0,0), fx = float(1/ceiling), fy = float(1/ceiling))
    cv2.imwrite(image, out_img)

def filterMatches(matches, ratio = 0.7):
    filteredMatches = []
    for m in matches:
        if len(m) == 2 and m[0].distance < m[1].distance * ratio:
            filteredMatches.append(m[0])    
    return filteredMatches

def findDimensions(image, homography):
    pt1 = np.ones(3, np.float32)
    pt2 = np.ones(3, np.float32)
    pt3 = np.ones(3, np.float32)
    pt4 = np.ones(3, np.float32)

    y, x = image.shape[:2]

    pt1[:2] = [0,0]
    pt2[:2] = [x,0]
    pt3[:2] = [0,y]
    pt4[:2] = [x,y]

    maxX = None
    maxY = None
    minX = None
    minY = None

    for pt in [pt1, pt2, pt3, pt4]:
        hp = np.matrix(homography, np.float32) * np.matrix(pt, np.float32).T
        hp_arr = np.array(hp, np.float32)
        normal_pt = np.array([hp_arr[0]/hp_arr[2], hp_arr[1]/hp_arr[2]], np.float32)

        if (maxX == None or normal_pt[0,0] > maxX):
            maxX = normal_pt[0,0]
        if (maxY == None or normal_pt[1,0] > maxY):
            maxY = normal_pt[1,0]
        if (minX == None or normal_pt[0,0] < minX):
            minX = normal_pt[0,0]
        if (minY == None or normal_pt[1,0] < minY):
            minY = normal_pt[1,0]

    minX = min(0, minX)
    minY = min(0, minY)
    return (minX, minY, maxX, maxY)

def calculateSize(baseImage,nextImage,homography):
    (minX, minY, maxX, maxY) = findDimensions(nextImage, homography)

    # Adjust max_x and max_y by base img size
    maxX = max(maxX, baseImage.shape[1])
    maxY = max(maxY, baseImage.shape[0])

    moveH = np.matrix(np.identity(3), np.float32)

    if (minX < 0):
        moveH[0,2] += -minX
        maxX += -minX

    if (minY < 0):
        moveH[1,2] += -minY
        maxY += -minY

    imgW = int(math.ceil(maxX))
    imgH = int(math.ceil(maxY))
    print "New Dimensions: ", (imgW, imgH)

    if (imgW > 10000 or imgH > 10000):
        raise Exception('Calculated size too large')
    return (moveH, imgW, imgH)

def stitchImages(baseImage, imageList, output, round):
    if (len(imageList) < 1):
        print "No more image...Panorama created ^_^"
        return baseImage

    detector = cv2.SIFT()

    baseImageGray = cv2.GaussianBlur(cv2.cvtColor(baseImage,cv2.COLOR_BGR2GRAY), (5,5), 0)
    try :
        baseFeatures, baseDescs = detector.detectAndCompute(baseImageGray, None)
    except cv2.error as e:
        e = str(e)
        if "Failed to allocate" in e:
            print "Panorama too big for further stitching. =("
            return baseImage

    FLANN_INDEX_KDTREE = 1  
    flannParams = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    matcher = cv2.FlannBasedMatcher(flannParams, {})

    closestImage = -1
    maxInlierRatio = 0

    # Find the best next image from the remaining images
    for nextImagePath in imageList:
        print 'Reading %s ......' % (nextImagePath)
        
        nextImageRbg = cv2.imread(nextImagePath)
        nextImage = cv2.GaussianBlur(cv2.cvtColor(nextImageRbg,cv2.COLOR_BGR2GRAY), (5,5), 0)
        nextFeatures, nextDescs = detector.detectAndCompute(nextImage, None)

        matches = matcher.knnMatch(nextDescs, trainDescriptors = baseDescs, k = 2)
        filteredMatches = filterMatches(matches)

        kp1 = []
        kp2 = []
        for match in filteredMatches:
            kp1.append(baseFeatures[match.trainIdx])
            kp2.append(nextFeatures[match.queryIdx])

        p1 = np.array([k.pt for k in kp1])
        p2 = np.array([k.pt for k in kp2])

        if len(p1) >= 10:
            H, status = cv2.findHomography(p1, p2, cv2.RANSAC, 5.0)
            print '%d / %d  inliers/matched' % (np.sum(status), len(status))

            inlierRatio = float(np.sum(status)) / float(len(status))
            matchedNumber = len(status)

            if (closestImage == -1 or inlierRatio > _inlierRatio):
                closestImage = 1
                _H = H
                _inlierRatio = inlierRatio
                _matchedNumber = matchedNumber
                _nextImagePath = nextImagePath
                _nextImageRbg = nextImageRbg
                _nextImage = nextImage
        else:
            print '%d matches found, not enough for homography estimation' % len(p1)


    print "Closest Image: ", _nextImagePath

    newImageList = filter(lambda x: x != _nextImagePath, imageList)

    # stitch img
    H = _H
    H_inv = linalg.inv(H)

    if (_inlierRatio > 0.1): 
        try : 
            (moveH, imgW, imgH) = calculateSize(baseImageGray,_nextImage,H_inv)
        except Exception as e:
            print e
            return baseImage
        modInvH = moveH * H_inv

        # Warp the new image based on the homography
        baseImageWarp = cv2.warpPerspective(baseImage, moveH, (imgW, imgH))
        nextImageWarp = cv2.warpPerspective(_nextImageRbg, modInvH, (imgW, imgH))

        panorama = np.zeros((imgH, imgW, 3), np.uint8)

        # Create a mask from the warped image for constructing panorama
        (ret,data_map) = cv2.threshold(cv2.cvtColor(nextImageWarp, cv2.COLOR_BGR2GRAY), 0, 255, cv2.THRESH_BINARY)
        panorama = cv2.add(panorama, baseImageWarp, mask = np.bitwise_not(data_map), dtype = cv2.CV_8U)
        resultImage = cv2.add(panorama, nextImageWarp, dtype = cv2.CV_8U)

        final_filename = "%s/%d.jpg" % (output, round)
        cv2.imwrite(final_filename, resultImage)
        print "Image stitched. Proceed to next image ^_^ \n"

        return stitchImages(resultImage, newImageList, output, round+1)
    else:
        return stitchImages(baseImage, newImageList, output, round+1)

if  __name__  == '__main__':
    output_dir = raw_input("Type in the output directory: ")
    img_dir = raw_input("Type in the source images directory: ")
    file_format = "." + raw_input("Type in the image file format: ")
    imageList = []
    os.chdir(img_dir)
    for files in os.listdir("."):
        if (files.endswith(file_format)):
            print "Checking image size..."
            resizeImage(files)
            imageList.append(files)

    base_img = raw_input("Type in the name of base image: ")
    baseImage = cv2.imread(base_img)
    panorama = stitchImages(baseImage, imageList, output_dir, 0)
    output_name = "panorama" + file_format
    cv2.imwrite('panorama.jpg', panorama)
    
    # output_dir = "C:/Users/chen/Documents/GitHub/3d-reconstruction/project/panorama"
    # imageList = []
    # os.chdir("C:/Users/chen/Documents/GitHub/3d-reconstruction/project/panorama")
    # for files in os.listdir("."):
    #     if (files.endswith(".JPG")):
    #         print "Checking image size..."
    #         resizeImage(files)
    #         imageList.append(files)

    # baseImage = cv2.imread('B (3).JPG')
    # panorama = stitchImages(baseImage, imageList, output_dir, 0)
    # cv2.imwrite('panorama.jpg', panorama)